import './App.css';
import {RouterProvider} from 'react-router-dom'
import router from './routers/router';

function App() {
  return (
    <div className="App">
      <RouterProvider router={router}/>
      {/* <TodoList /> */}
    </div>
  );
}

export default App;

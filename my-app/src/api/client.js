import axios from "axios";

const client = axios.create({
    baseURL: "https://6566e57564fcff8d730f40dd.mockapi.io/api"
})

export default client
import client from "./client"

export const getTodos = () =>{
    return client.get("/todos")
}

export const postTodos = (data) => {
    return client.post("/todos",data)
}

export const deleteTodos = (id) => {
    return client.delete(`/todos/${id}`)
}

export const putTodos = (id,data) => {
    return client.put(`/todos/${id}`,data)
}
import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

export const todoSlice = createSlice({
    name: 'todos',
    initialState: {
        todos: [
            {id: uuidv4(),
                text: 'asadasd',
                done: true}
        ],
    },

    reducers: {
        addTodo: (state, action) => {
            state.todos.push(action.payload)
        },
        deleteTodo: (state, action) => {
            state.todos = state.todos.filter(todo => todo.id !== action.payload)
        },
        toggleTodo: (state, action) => {
            const todo = state.todos.find(todo => todo.id === action.payload)
            if (todo) {
                todo.done = !todo.done
            }
        },
        setTodos: (state,action) => {
            state.todos = action.payload
        },
        editTodo: (state, action) => {
            const todo = state.todos.find(todo => todo.id === action.payload.id);
            if (todo) {
                todo.text = action.payload.text;
            }
        }
    }

})

export const { addTodo, deleteTodo, toggleTodo,setTodos,editTodo } = todoSlice.actions

export default todoSlice.reducer
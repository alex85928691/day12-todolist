import { useSelector } from "react-redux/es/hooks/useSelector"
import { Link } from "react-router-dom"
const TodoDonefilter = () =>{
    const todos = useSelector(state => state.todos.todos)
    return (
        <div className="todoDone">
            <h1>Done List</h1>
            {
            todos.filter(todo => todo.done).map(todo =>{
                return <p>
                    <Link to={"/todos/"+todo.id}>{todo.text}</Link>
                    </p>
            }
                )
        }</div>
    )
}

export default TodoDonefilter;
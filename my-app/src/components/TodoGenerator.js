import { useState } from "react";
import { Input, Button } from "antd";
import { useTodos } from "./hooks/useTodos";
import { PlusOutlined } from "@ant-design/icons"

const TodoGenerator = () => {
    const [todoText, setTodoText] = useState('')
    const { addTodos } = useTodos()
    const handleSubmit = (event) => {
        event.preventDefault()
        if (!todoText) return
        const newTodo = {
            text: todoText,
            done: false
        }
        addTodos(newTodo)
        setTodoText('')
    }
    return (
        <form >
            <Input className="input" size="large" type="text" value={todoText} onChange={e => setTodoText(e.target.value)} placeholder="input a new todo item" />
            <Button className="submitButton" onClick={handleSubmit} shape="default" icon={<PlusOutlined />} >
                add
            </Button>
        </form>
    )
}

export default TodoGenerator
import { useSelector } from 'react-redux'
import TodoItem from "./TodoItem"
const TodoGroup = () => {
    const todos = useSelector(state => state.todos.todos)
    return (
        <div className='todoGroup'>
            {todos.map((todo) => {
                return <TodoItem key={todo.id} todo={todo} />
            })}
        </div>


    )

}

export default TodoGroup
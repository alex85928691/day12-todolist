import { useDispatch } from "react-redux";
import { deleteTodo, editTodo } from "../app/todoSlice";
import { deleteTodos, putTodos } from "../api/todos";
import { useTodos } from './hooks/useTodos'
import { useState } from "react";
import { Modal, Input } from 'antd';
import { EditOutlined, CloseOutlined } from '@ant-design/icons'

const TodoItem = ({ todo }) => {
    const { toggleTodos } = useTodos();
    const dispatch = useDispatch()
    const handleDelete = () => {
        Modal.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to delete this todo?',
            onOk() {
                deleteTodos(todo.id).then(() => {
                    dispatch(deleteTodo(todo.id));
                }).catch(error => console.error(error));
            },
        });
    }

    const handleToggle = () => {
        const newTodo = {
            id: todo.id,
            text: todo.text,
            done: !todo.done
        }
        toggleTodos(todo.id, newTodo)
    }

    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [updatedText, setUpdatedText] = useState(todo.text);

    const openModal = () => {
        setModalIsOpen(true);
    }

    const handleCloseModal = () => {
        setUpdatedText(todo.text);
        setModalIsOpen(false);
    };

    const handleEdit = () => {
        const updatedTodo = {
            id: todo.id,
            text: updatedText,
            done: todo.done
        };
        putTodos(todo.id, updatedTodo).then(() => {
            dispatch(editTodo(updatedTodo));
        }).catch((error) => {
            console.error(error);
        });
        setModalIsOpen(false);
    }
    
    return (
        <div className='todoItem'>
            <p onClick={handleToggle} style={{ textDecoration: todo.done ? 'line-through' : 'none' }}>{todo.text}</p>
            <CloseOutlined onClick={handleDelete} />
            <EditOutlined onClick={openModal} />
            <Modal
                title="TodoItem"
                visible={modalIsOpen}
                onOk={handleEdit}
                onCancel={handleCloseModal}
                okText="OK"
                cancelText="Cancel"
                okButtonProps={{ disabled: updatedText.trim() === "" }}
            >
                <Input
                    value={updatedText}
                    onChange={e => setUpdatedText(e.target.value)}
                />
            </Modal>
        </div>
    )

}

export default TodoItem
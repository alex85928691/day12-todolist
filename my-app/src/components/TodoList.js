import { useEffect } from 'react'
import TodoGenerator from './TodoGenerator'
import TodoGroup from './TodoGroup'
import { useTodos } from './hooks/useTodos'



const TodoList = () => {
    const { loadTodos } = useTodos();
    useEffect(()=>{
        loadTodos()
    })
    
    return (
        <div>
            <h1>TODO List</h1>
            <TodoGroup></TodoGroup>
            <TodoGenerator></TodoGenerator>
        </div>
        
    )
}

export default TodoList
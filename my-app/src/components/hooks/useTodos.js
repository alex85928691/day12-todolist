import { useDispatch } from "react-redux"
import { getTodos, putTodos ,postTodos} from "../../api/todos"
import { setTodos } from "../../app/todoSlice"

export const useTodos = () => {
    const dispatch = useDispatch()
    const loadTodos = () => {
        getTodos().then(response => {
            dispatch(setTodos(response.data))
        })
    }

    const addTodos = async (data) =>{
        await postTodos(data)
        loadTodos()
    }

    const toggleTodos = async (id,data) => {
        await putTodos(id,data)
        loadTodos()
    }

    return{
        loadTodos,addTodos,toggleTodos
    }
}
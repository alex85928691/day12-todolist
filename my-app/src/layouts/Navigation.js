import { Link } from 'react-router-dom';
import { Menu } from 'antd';

const Navigation = () => {
    return (
        <nav>
            <Menu mode="horizontal" className="menu" defaultSelectedKeys={["home"]}>
            <Menu.Item key="home">
                <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item key="done">
                <Link to="/done">Done</Link>
            </Menu.Item>
            <Menu.Item key="about">
                <Link to="/about">About</Link>
            </Menu.Item>
        </Menu>
        </nav>
        
    );
};

export default Navigation;
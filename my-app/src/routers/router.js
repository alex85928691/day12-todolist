import TodoList from '../components/TodoList';
import {createBrowserRouter} from 'react-router-dom'
import AboutPage from '../pages/AboutPage';
import TodoDonefilter from '../components/TodoDonefilter'
import Layout from '../layouts/Layout';
import TodoDoneItem from "../components/TodoDoneItem"
import NotFoundPage from '../pages/NotFoundPage';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <NotFoundPage />,
        children:[{
            path: "/",
            element: <TodoList />,
        },{
            path: "/done",
            element: <TodoDonefilter />,
        },
        {
            path: "/about",
            element: <AboutPage />,
        },
        {
            path: "/todos/:id",
            element: <TodoDoneItem />
        }]
    },
    
])

export default router